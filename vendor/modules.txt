# github.com/mattn/go-sqlite3 v1.14.9
## explicit; go 1.12
github.com/mattn/go-sqlite3
# gopkg.in/yaml.v2 v2.4.0
## explicit; go 1.15
gopkg.in/yaml.v2
