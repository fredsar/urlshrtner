package urlshort

import (
	"encoding/json"
	"net/http"

	"gitlab.com/fredsar/urlshrtner/internal/models"
	"gopkg.in/yaml.v2"
)

type DBHandler interface {
	GetPathsURL() ([]models.PathURL, error)
}

type Handler struct {
	dbHandler DBHandler
}

func NewHandler(dbH DBHandler) Handler {
	return Handler{
		dbHandler: dbH,
	}
}

// MapHandler will return an http.HandlerFunc (which also
// implements http.Handler) that will attempt to map any
// paths (keys in the map) to their corresponding URL (values
// that each key in the map points to, in string format).
// If the path is not provided in the map, then the fallback
// http.Handler will be called instead.
func (h Handler) MapHandler(pathsToUrls map[string]string, fallback http.Handler) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) { //nolint: varnamelen
		path := r.URL.Path
		if urlP, ok := pathsToUrls[path]; ok {
			http.Redirect(rw, r, urlP, http.StatusPermanentRedirect)

			return
		}
		fallback.ServeHTTP(rw, r)
	}
}

// YAMLHandler will parse the provided YAML and then return
// an http.HandlerFunc (which also implements http.Handler)
// that will attempt to map any paths to their corresponding
// URL. If the path is not provided in the YAML, then the
// fallback http.Handler will be called instead.
//
// YAML is expected to be in the format:
//
//     - path: /some-path
//       url: https://www.some-url.com/demo
//
// The only errors that can be returned all related to having
// invalid YAML data.
//
// See MapHandler to create a similar http.HandlerFunc via
// a mapping of paths to urls.
func (h Handler) YAMLHandler(yamlBytes []byte, fallback http.Handler) (http.HandlerFunc, error) {
	pathURLs, err := parseYaml(yamlBytes)
	if err != nil {
		return nil, err
	}
	pathsToURLs := buildMap(pathURLs)

	return h.MapHandler(pathsToURLs, fallback), nil
}

func (h Handler) JSONHandler(jsonBytes []byte, fallback http.Handler) (http.HandlerFunc, error) {
	pathURLs, err := parseJSON(jsonBytes)
	if err != nil {
		return nil, err
	}
	pathsToURLs := buildMap(pathURLs)

	return h.MapHandler(pathsToURLs, fallback), nil
}

func (h Handler) SQLHandler(fallback http.Handler) (http.HandlerFunc, error) {
	pathsUrls, err := h.dbHandler.GetPathsURL()
	if err != nil {
		return nil, err
	}
	pathsToURLs := buildMap(pathsUrls)

	return h.MapHandler(pathsToURLs, fallback), nil
}

func buildMap(pathUrls []models.PathURL) map[string]string {
	pathsToUrls := make(map[string]string)
	for _, pu := range pathUrls {
		pathsToUrls[pu.Path] = pu.URL
	}

	return pathsToUrls
}

func parseYaml(data []byte) ([]models.PathURL, error) {
	pathUrls := make([]models.PathURL, 0)
	if err := yaml.Unmarshal(data, &pathUrls); err != nil {
		return nil, err
	}

	return pathUrls, nil
}

func parseJSON(data []byte) ([]models.PathURL, error) {
	pathURLs := make([]models.PathURL, 0)
	if err := json.Unmarshal(data, &pathURLs); err != nil {
		return nil, err
	}

	return pathURLs, nil
}
