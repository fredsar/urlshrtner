package database

import (
	"database/sql"
	"log"

	_ "github.com/mattn/go-sqlite3" // the sqlLite library
	"gitlab.com/fredsar/urlshrtner/internal/models"
)

type DB struct {
	db *sql.DB
}

func OpenDBConnection(pathToDB string) (DB, func() error, error) {
	sqlDB, err := sql.Open("sqlite3", pathToDB)
	if err != nil {
		return DB{}, nil, err
	}

	return DB{
		db: sqlDB,
	}, sqlDB.Close, nil
}

func (d DB) GetPathsURL() ([]models.PathURL, error) {
	rows, err := d.db.Query(
		`SELECT 
			path, 
			url
		FROM
			pathurls`,
	)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.Println(err)
		}
	}()

	pathsURLs := make([]models.PathURL, 0)
	for rows.Next() {
		var pathURL models.PathURL
		err = rows.Scan(&pathURL.Path, &pathURL.URL)
		if err != nil {
			return nil, err
		}
		pathsURLs = append(pathsURLs, pathURL)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return pathsURLs, nil
}
