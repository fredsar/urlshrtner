package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/fredsar/urlshrtner/internal/database"
	"gitlab.com/fredsar/urlshrtner/internal/urlshort"
)

type configFlags struct {
	YamlFile string
	JSONFile string
	DBFile   string
}

func main() {
	config := parseFlags()
	mux := defaultMux()

	dbI, closeBD, err := database.OpenDBConnection(config.DBFile)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := closeBD(); err != nil {
			log.Println(err)
		}
	}()
	handler := urlshort.NewHandler(dbI)

	// Build the MapHandler using the mux as the fallback
	pathsToUrls := map[string]string{
		"/urlshort-godoc": "https://godoc.org/github.com/gophercises/urlshort",
		"/yaml-godoc":     "https://godoc.org/gopkg.in/yaml.v2",
	}
	mapHandler := handler.MapHandler(pathsToUrls, mux)

	ymlFileBytes, err := parseFile(config.YamlFile)
	if err != nil {
		log.Panic(err)
	}
	yamlHandler, err := handler.YAMLHandler(ymlFileBytes, mapHandler)
	if err != nil {
		log.Panic(err)
	}

	jsonFileBytes, err := parseFile(config.JSONFile)
	if err != nil {
		log.Panic(err)
	}
	jsonHandler, err := handler.JSONHandler(jsonFileBytes, yamlHandler)
	if err != nil {
		log.Panic(err)
	}

	dbHandler, err := handler.SQLHandler(jsonHandler)
	if err != nil {
		log.Panic(err)
	}

	log.Println("Starting the server on :8080")
	err = http.ListenAndServe(":8080", dbHandler)
	if err != nil {
		log.Panic(err)
	}
}

func defaultMux() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/", hello)

	return mux
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, world!")
}

func parseFlags() configFlags {
	ymlFile := flag.String("ymlfile", "routes.yml", "The YML file to fetch the routes")
	jsonFile := flag.String("jsonfile", "routes.json", "The JSON file to fetch the routes")
	dbFile := flag.String("dbfile", "routes.db", "The SQLite BD file to fetch the routes")
	flag.Parse()

	return configFlags{
		YamlFile: *ymlFile,
		JSONFile: *jsonFile,
		DBFile:   *dbFile,
	}
}

func parseFile(path string) ([]byte, error) {
	fileBytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	return fileBytes, nil
}
