module gitlab.com/fredsar/urlshrtner

go 1.17

require (
	github.com/mattn/go-sqlite3 v1.14.9
	gopkg.in/yaml.v2 v2.4.0
)
